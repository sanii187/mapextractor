﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapExtractor
{
    public class AreaData
    {
        #region Constants

        private const string StartToken = "Start Object";
        private const string EndToken = "End Object";
        private const string ObjectCountToken = "ObjectCount";

        #endregion

        #region Constructor

        public AreaData(string filePath)
        {
            AreaDataObjects = new List<AreaDataObject>();
            Load(filePath);
        }

        #endregion

        #region Properties & Members

        public List<AreaDataObject> AreaDataObjects { get; }

        #endregion

        #region Methods

        private void Load(string filePath)
        {
            try
            {
                int groupLineIndex = 0;
                AreaDataObject areaDataObject = null;

                foreach (string line in File.ReadAllLines(filePath))
                {
                    if (string.IsNullOrEmpty(line))
                    {
                        continue;
                    }

                    if (line.StartsWith(ObjectCountToken) && int.TryParse(line.Replace(ObjectCountToken, string.Empty).Trim(), out int objectCount))
                    {
                        if (objectCount != AreaDataObjects.Count)
                        {
                            Program.Log($"{filePath} ObjectCount ({objectCount} != {AreaDataObjects.Count})");
                        }
                    }

                    if (line.StartsWith(StartToken) && int.TryParse(line.Replace(StartToken, string.Empty), out int parsedID))
                    {
                        areaDataObject = new AreaDataObject();
                        areaDataObject.ID = parsedID;
                    }

                    if (areaDataObject != null)
                    {
                        if (groupLineIndex == 2 && long.TryParse(line.Trim(), out long parsedPropertyID))
                        {
                            areaDataObject.PropertyID = parsedPropertyID;
                        }

                        if (line.StartsWith(EndToken))
                        {
                            AreaDataObjects.Add(areaDataObject);
                            areaDataObject = null;
                            groupLineIndex = 0;
                        }
                        else
                        {
                            groupLineIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Program.Log(ex.Message);
                return;
            }
            finally
            {
                
            }
        }

        #endregion
    }
}
