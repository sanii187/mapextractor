﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MapExtractor
{
    class Program
    {
        #region Logging

        public static string LogPath = Path.Combine(Application.StartupPath, "log.txt");

        public static void Log(string message)
        {
            Log(new string[] { message });
        }

        public static void Log(IEnumerable<string> messages)
        {
            File.AppendAllLines(LogPath, messages);

            foreach (string message in messages)
            {
                Console.WriteLine(message);
            }
        }

        public static void ClearLogs()
        {
            if (!File.Exists(LogPath))
            {
                return;
            }

            File.Delete(LogPath);
        }

        #endregion

        #region Main

        static void Main(string[] args)
        {
            ClearLogs();

            if (args.Any())
            {
                using (ExtractionHandler handler = new ExtractionHandler(args))
                {
                    Log("Finished extraction.");
                }
            }
            else
            {
                Console.WriteLine("To extract all map related stuff just drag the map folder on the application.");
                Console.ReadLine();
            }
        }

        #endregion
    }
}
