﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MapExtractor
{
    public class MapFolder
    {
        #region Constructor

        public MapFolder(string folderPath)
        {
            AreaDatas = new List<AreaData>();
            UsedPropertyFiles = new List<Property>();
            FolderPath = folderPath;
            Load();
        }

        #endregion

        #region Methods

        private void Load()
        {
            AreaDatas.Clear();
            UsedPropertyFiles.Clear();

            string[] files = Directory.GetFiles(FolderPath, "areadata.txt", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                AreaData areaData = new AreaData(file);
                AreaDatas.Add(areaData);

                foreach (AreaDataObject areaDataObject in areaData.AreaDataObjects)
                {
                    if (!UsedPropertyFiles.Any(p => p.ID == areaDataObject.PropertyID))
                    {
                        Property property = ExtractionHandler.GetProperty(areaDataObject.PropertyID);
                        if (property != null)
                        {
                            UsedPropertyFiles.Add(property);
                        }
                    }
                }
            }
        }

        public void Extract()
        {
            try
            {
                string extractPath = Path.Combine(Application.StartupPath, $"extract{DateTime.Now:_yyyyMMdd_HH_mm}");
                if (!Directory.Exists(extractPath))
                {
                    Directory.CreateDirectory(extractPath);
                }

                foreach (Property property in UsedPropertyFiles)
                {
                    FileInfo propertyFileInfo = new FileInfo(property.OwnFilePath);
                    string targetFile = property.OwnFilePath.Replace(Application.StartupPath, extractPath);
                    string targetFolder = propertyFileInfo.DirectoryName.Replace(Application.StartupPath, extractPath);

                    if (!Directory.Exists(targetFolder))
                    {
                        Directory.CreateDirectory(targetFolder);
                    }

                    File.Copy(property.OwnFilePath, targetFile, true);
                }
            }
            catch (Exception ex)
            {
                Program.Log(ex.Message);
                return;
            }
            finally
            {

            }
        }

        #endregion

        #region Properties & Members

        public string FolderPath { get; }

        public List<AreaData> AreaDatas { get; }
        public List<Property> UsedPropertyFiles { get; }

        #endregion

    }
}
