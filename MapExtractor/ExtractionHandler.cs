﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MapExtractor
{
    public class ExtractionHandler : IDisposable
    {
        #region Constants & Static Members

        private static string PropertyPath = Path.Combine(Application.StartupPath, "property");

        public static Property GetProperty(long ID)
        {
            return PropertyFiles.FirstOrDefault(p => p.ID == ID);
        }

        #endregion

        #region Constructor

        public ExtractionHandler(string[] mapFolderPaths)
        {
            LoadProperties();

            foreach (string arg in mapFolderPaths)
            {
                MapFolder folder = new MapFolder(arg);
                folder.Extract();
            }
        }

        #endregion

        #region Properties

        public static List<Property> PropertyFiles;

        #endregion

        #region Methods

        private void LoadProperties()
        {
            Program.Log("Start loading property files ...");

            PropertyFiles = new List<Property>();
            try
            {
                string[] files = Directory.GetFiles(PropertyPath, "*.pr*", SearchOption.AllDirectories);
                foreach (string file in files)
                {
                    PropertyFiles.Add(new Property(file));
                }
            }
            catch (Exception ex)
            {
                Program.Log(ex.Message);
                Program.Log("Canceled loading property files!");
            }
            finally
            {
                Program.Log("Finished loading property files!");
            }
        }


        public void Dispose()
        {
        }

        #endregion
    }
}
