﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MapExtractor
{
    public enum PropertyType
    {
        Ambience,
        Building,
        DungeonBlock,
        Effect,
        Tree,
    }

    public class Property
    {
        #region Constants

        private const string AmbienceExtension = ".pra";
        private const string BuildingExtension = ".prb";
        private const string TreeExtension = ".prt";
        private const string EffectExtension = ".pre";
        private const string DungeonBlockExtension = ".prd";

        private const string PropertyNameToken = "propertyname";

        #endregion

        #region Constructor

        public Property(string filePath)
        {
            Load(filePath);
        }

        #endregion

        #region Properties

        public long ID { get; set; }
        public PropertyType? Type { get; set; }
        public string Name { get; set; }
        public string OwnFilePath { get; set; }
        public string UsedFilePath { get; set; }

        #endregion

        #region Methods

        private void Load(string filePath)
        {
            OwnFilePath = filePath;
            FileInfo fileInfo = new FileInfo(filePath);

            string fileToken = string.Empty;

            switch (fileInfo.Extension)
            {
                case AmbienceExtension:
                    fileToken = "ambiencesoundvector";
                    Type = PropertyType.Ambience;
                    break;
                case BuildingExtension:
                    fileToken = "buildingfile";
                    Type = PropertyType.Building;
                    break;
                case DungeonBlockExtension:
                    fileToken = "dungeonblockfile";
                    Type = PropertyType.DungeonBlock;
                    break;
                case EffectExtension:
                    fileToken = "effectfile";
                    Type = PropertyType.Effect;
                    break;
                case TreeExtension:
                    fileToken = "treefile";
                    Type = PropertyType.Tree;
                    break;
            }

            int lineIndex = 0;
            foreach (string line in File.ReadAllLines(filePath, Encoding.Default))
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }

                if (lineIndex == 1 && long.TryParse(line.Trim(), out long parsedID))
                {
                    ID = parsedID;
                }

                if (line.StartsWith(PropertyNameToken))
                {
                    Name = line.Replace(PropertyNameToken, string.Empty).Replace("\"", string.Empty).Trim();
                }

                if (line.StartsWith(fileToken))
                {
                    UsedFilePath = line.Replace(fileToken, string.Empty).Replace("\"", string.Empty).Trim();
                }

                lineIndex++;
            }
        }

        public override string ToString()
        {
            return $"{Type}: {Name} - {UsedFilePath} ({OwnFilePath})";
        }

        #endregion
    }
}
