﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MapExtractor
{
    public class AreaDataObject
    {
        #region Constructor

        public AreaDataObject()
        {
        }

        #endregion

        #region Properties & Members

        public int ID { get; set; }
        public decimal X { get; set; }
        public decimal Y { get; set; }
        public decimal Z { get; set; }
        public long PropertyID { get; set; }

        #endregion
    }
}
